"""Posts URLs."""

# Django
from django.urls import path

# Views
from choferes import views

urlpatterns = [
    path(
        route='',
        view=views.ChoferesView.as_view(),
        name='list'
    ),
    path(
        route='<int:pk>',
        view=views.DetailChoferView.as_view(),
        name='detalle'
    ),
    path(
        route='create',
        view=views.CreateChoferView.as_view(),
        name='crear'
    ),
    path(
        route='editar/<int:pk>',
        view=views.UpdateChoferView.as_view(),
        name='editar'
    ),
    path(
        route='eliminar/<int:pk>',
        view=views.DeleteChoferView.as_view(),
        name='eliminar'
    ),

]
