"""Post forms."""

# Django
from django import forms

# Models
from choferes.models import Chofer


class ChoferForm(forms.ModelForm):

    class Meta:
        model = Chofer
        fields = ('rut', 'nombre', 'apellido', 'telefono')