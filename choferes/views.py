# Create your views here.

# Django
from django.shortcuts import render
from django.urls import reverse_lazy
#from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import CreateView, DetailView, ListView, UpdateView, DeleteView
from django.template import loader, Context

# Forms
#from posts.forms import PostForm
from choferes.forms import ChoferForm

# Models
from choferes.models import Chofer


class ChoferesView(ListView):

    template_name = 'choferes/index.html'
    model = Chofer
    ordering = ('-created',)
    paginate_by = 30
    #context_object_name = 'posts'

class DetailChoferView(DetailView):

    model = Chofer
    template_name = 'choferes/view.html'
    #queryset = Post.objects.all()

class CreateChoferView(CreateView):

    model = Chofer
    form_class = ChoferForm
    template_name = 'choferes/create.html'
    success_url = reverse_lazy('choferes:list')

class UpdateChoferView(UpdateView):
    model = Chofer
    template_name = 'choferes/update.html'
    form_class = ChoferForm
    success_url = reverse_lazy('choferes:list')

class DeleteChoferView(DeleteView):

    model = Chofer
    form_class = ChoferForm
    success_url = reverse_lazy('choferes:list')

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)







