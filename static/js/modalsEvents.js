$(document).ready(function () {
    $('#modal-form').modal();
    $('#modal-msg').modal();
    $('#modal-confirm').modal();

    $('.form-modal-cancel').click(function (e) {
        e.preventDefault();
        $('#modal-confirm').modal('close');
        $('#modal-form').modal('close');
    });

    $('.test-btn').click(function (e) {
        e.preventDefault();
        $('#modal-form').modal('open');
    });


    //Confirm delete
    $('.btn-confirm').click(function (e) {
        e.preventDefault();
        let url = $(this).attr('href');
        $('#modal-confirm').modal('open');
        $('.modal-confirm-submit').click(function () {
            window.location.href = url;
            return true;
        });
    });
});