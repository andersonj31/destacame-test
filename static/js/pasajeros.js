$(function () {

    /*Global vars*/
    var trayecto_id = null;
    var trayecto_bus = null;
    var numAsiento = null;
    

    $('#trayecto').change(function (e) { 
        e.preventDefault();
        $('.msg-error').html("");
        $('#trayecto_bus').html("");
        $('.area-asientos').html("");
        $('.horarios_buses').hide();
        $('.asientos').hide();
        trayecto_id = $(this).val()
        $.ajax({            
            type: "POST",  
            url: "/pasajeros/getTrayectosBuses",
            data: {"trayecto_id":trayecto_id,
                csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value
            } ,
            success: function(result){  
                if(result == null || result == ''){
                    $('.msg-error').html("No hay viajes disponibles para este trayecto, por favor intente mas tarde.");
                }else{
                    var html = "<option value='' disabled selected>Selecione una opcion</option>";
                    $.each( result, function( key, value ) {
                        html += "<option value="+value.pk+">"+value.fields.hora_salida+" - "+value.fields.hora_llegada+"</option>"
                    });
                    $('#trayecto_bus').html(html);
                    $('select').formSelect();
                    $('.horarios_buses').show();
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) { 
                alert("Status: " + textStatus); alert("Error: " + errorThrown); 
            }       
        });
    });


    $('#trayecto_bus').change(function (e) { 
        e.preventDefault();
        $('.msg-error-asientos').html("");
        $('.area-asientos').html("");
        $('.asientos').hide();
        trayecto_bus = $(this).val()
        $.ajax({            
            type: "POST",  
            url: "/pasajeros/getAsientos",
            data: {"trayecto_bus":trayecto_bus,
                csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value
            } ,
            success: function(result){  
                    var asientosReservados = []
                    $.each( result, function( key, value ) {
                        asientosReservados.push(value.fields.num_asiento)
                    });
                    if(asientosReservados.length == 10){
                        $('.msg-error-horarios').html("No hay asientos disponibles para este horario, por favor intente mas tarde.");
                    }else{
                    var html = '<div class="row" >';
                        for (var i = 1; i < 11; i++) {
                            let status = asientosReservados.includes(i) ? 'disabled':''

                            html += "<div class='col s1'>"
                            html += "<button class='asientos btn' data-id='"+i+"' "+ status +">"
                            html += "<i class='small material-icons dp48 '>airline_seat_recline_extra</i>"
                            html += "<span>N°"+i+" </span></button></div>"
                         }
                         html += "</div>"
                    $('.area-asientos').html(html);
                    $('.asientos').show();
                    }
                
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) { 
                alert("Status: " + textStatus); alert("Error: " + errorThrown); 
            }       
        });
    });


    $(document).on("click",".asientos",function(e) {
        e.preventDefault();
        e.stopPropagation();
        numAsiento = $(this).data('id')
        $('#num_asiento').val(numAsiento);
    });

    $('.submit').click(function (e) { 
        e.preventDefault();
        if(numAsiento != null && trayecto_id != null && trayecto_bus != null){
            $('.pasajero-form').submit();
        }else{
            $('.modal-msg-title').html("Error en validaciones");
            $('.modal-msg-body').html("Por favor ingrese un horario y numero de asiento valido.");
            $('#modal-msg').modal('open');
        }
    });
});

