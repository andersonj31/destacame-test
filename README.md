## Prueba Test Empresa Destacame. ##

#El sistema fue desarrollado en un entorno virtual propocionado por django dentro del equipo local.

## Instalación

1. `sudo apt install python3.6`
2. `sudo apt install python3-pip`

## Entorno virtual

1.  `python3 -m venv Django` donde `Django` sea el nombre deseado
2.  `source Django/bin/active` para activar el entorno
3.  `deactivate` para desactivar el entorno

## Instalación de django

1. Activar entorno virtual
2. `pip install django`
3. `pip install mysqlclient`

# Crear Base de datos y modificar los valores en settings.py

'NAME':  'destacame',
'HOST': 'localhost',
'USER': 'root',
'PASSWORD': '******',

# Ejecutar las migracion

1. `python3 manage.py makemigrations`
2. `python3 manage.py migrate`

# Inicializar Servidor Local

1. `python3 manage.py runserver`

# Acceder a la app mediante la url

`http://localhost:8000/choferes/`
