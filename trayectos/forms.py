"""Post forms."""

# Django
from django import forms

# Models
from trayectos.models import Trayecto, TrayectoBuses


class TrayectoForm(forms.ModelForm):

    class Meta:
        model = Trayecto
        fields = ('ruta_salida','ruta_llegada','descripcion')

class HorariosForm(forms.ModelForm):
    class Meta:

        model = TrayectoBuses
        fields = [
			'hora_salida',
			'hora_llegada',
			'descripcion',
			'bus',
			'trayecto',
		]