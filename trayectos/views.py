# Django
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import CreateView, DetailView, ListView, UpdateView, DeleteView
from django.template import loader, Context
# Forms
from trayectos.forms import TrayectoForm, HorariosForm

# Models
from trayectos.models import Trayecto, TrayectoBuses
from buses.models import Bus
from pasajeros.models import Pasajero

class TrayectoView(ListView):

    template_name = 'trayectos/index.html'
    model = Trayecto
    ordering = ('-created',)
    paginate_by = 30
    context_object_name = 'trayectos'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        trayectos = Trayecto.objects.all()
        totalTrayectos = trayectos.count()
        for trayecto in trayectos:
            horarios =  TrayectoBuses.objects.filter(trayecto = trayecto.pk)
            trayecto.prom =  round(Pasajero.objects.filter(trayecto_bus__in = horarios).count() / totalTrayectos, 2)
        context['trayectos'] = trayectos

        return context


class DetailTrayectoView(DetailView):

    model = Trayecto
    template_name = 'trayectos/view.html'

class CreateTrayectoView(CreateView):

    model = Trayecto
    form_class = TrayectoForm
    template_name = 'trayectos/create.html'
    success_url = reverse_lazy('trayectos:list')


class UpdateTrayectoView(UpdateView):

    model = Trayecto
    template_name = 'trayectos/update.html'
    form_class = TrayectoForm
    success_url = reverse_lazy('trayectos:list')

class DeleteTrayectoView(DeleteView):

    model = Trayecto
    form_class = TrayectoForm
    success_url = reverse_lazy('trayectos:list')

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

def asignarHorario(request,pk):
    trayecto = Trayecto.objects.get(id=pk)
    buses = Bus.objects.all()
    horariosAsigandos = TrayectoBuses.objects.filter(trayecto_id = pk)
    error_msg = False
    form = HorariosForm        
    if request.method == 'POST':
        cargado = TrayectoBuses.objects.filter(trayecto_id = pk,bus_id = request.POST['bus'] ).count()
        if cargado == 0:
            form = HorariosForm(request.POST)
            if form.is_valid():
                form.save()
        else:
            error_msg = "El Bus ya se encuentra asignado a un horario"

    contexto = {'form':form,
        'trayecto':trayecto,
        'buses':buses,
        'horariosAsigandos':horariosAsigandos,
        'error_msg': error_msg 
    }
    return render(request, 'trayectos/horarios.html', contexto)

def borrarHorario(request,trayecto,horario):
    TrayectoBuses.objects.get(id = horario).delete()
    return redirect('trayectos:asignar',trayecto)

