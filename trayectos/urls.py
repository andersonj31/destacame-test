"""Posts URLs."""

# Django
from django.urls import path

# Views
from trayectos import views

urlpatterns = [
    path(
        route='',
        view=views.TrayectoView.as_view(),
        name='list'
    ),
    path(
        route='<int:pk>',
        view=views.DetailTrayectoView.as_view(),
        name='detalle'
    ),
    path(
        route='crear',
        view=views.CreateTrayectoView.as_view(),
        name='crear'
    ),
    path(
        route='editar/<int:pk>',
        view=views.UpdateTrayectoView.as_view(),
        name='editar'
    ),
    path(
        route='eliminar/<int:pk>',
        view=views.DeleteTrayectoView.as_view(),
        name='eliminar'
    ),
    path(
        route='asignar/<int:pk>',
        view=views.asignarHorario,
        name='asignar'
    ),
    path(
        route='horario/borrar/<int:trayecto>/<int:horario>',
        view=views.borrarHorario,
        name='borrar_horario'
    ),

]
