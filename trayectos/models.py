from django.db import models

# Create your models here.
class Trayecto(models.Model):

    ruta_salida = models.CharField(max_length=50)
    ruta_llegada = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=50)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

class TrayectoBuses(models.Model):

    hora_salida = models.CharField(max_length=50)
    hora_llegada = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=50)
    bus = models.ForeignKey(
        'buses.Bus',
        on_delete=models.CASCADE,
    ) 
    trayecto = models.ForeignKey(
        Trayecto,
        on_delete=models.CASCADE,
    ) 
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)