"""Post forms."""

# Django
from django import forms

# Models
from buses.models import Bus


class BusForm(forms.ModelForm):

    class Meta:
        model = Bus
        fields = ('descripcion','chofer')