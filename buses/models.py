from django.db import models

# Create your models here.

class Bus(models.Model):

    descripcion = models.CharField(max_length=15)
    chofer = models.ForeignKey(
        'choferes.Chofer',
        on_delete=models.CASCADE,
    )   
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)