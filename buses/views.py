# Create your views here.

# Django
from django.shortcuts import render
from django.urls import reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import CreateView, DetailView, ListView, UpdateView, DeleteView
from django.template import loader, Context
# Forms
from buses.forms import BusForm
from choferes.forms import ChoferForm

# Models
from buses.models import Bus
from choferes.models import Chofer
from trayectos.models import TrayectoBuses, Trayecto
from pasajeros.models import Pasajero


class BusView(ListView):

    template_name = 'buses/index.html'
    model = Bus
    ordering = ('-created',)
    paginate_by = 30
    context_object_name = 'buses'


class DetailBusView(DetailView):

    model = Bus
    template_name = 'buses/view.html'

class CreateBusView(CreateView):

    model = Bus
    form_class = BusForm
    template_name = 'buses/create.html'
    success_url = reverse_lazy('buses:list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['chofer'] = Chofer.objects.all()
        return context
 


class UpdateBusView(UpdateView):

    model = Bus
    template_name = 'buses/update.html'
    form_class = BusForm
    success_url = reverse_lazy('buses:list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['chofer'] = Chofer.objects.all()
        return context

class DeleteBusView(DeleteView):

    model = Bus
    form_class = BusForm
    success_url = reverse_lazy('buses:list')

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

def busesFiltro(request):
    buses = Bus.objects.all()
    for bus in buses:
            horarios =  TrayectoBuses.objects.filter(bus = bus.pk)
            cantBoletos =  Pasajero.objects.filter(trayecto_bus__in = horarios).count()
            bus.vendido = True if cantBoletos >= 1 else False

    return render(request, 'buses/filter.html', {'buses':buses})