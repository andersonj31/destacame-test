"""Posts URLs."""

# Django
from django.urls import path

# Views
from buses import views

urlpatterns = [
    path(
        route='',
        view=views.BusView.as_view(),
        name='list'
    ),
    path(
        route='<int:pk>',
        view=views.DetailBusView.as_view(),
        name='detalle'
    ),
    path(
        route='crear',
        view=views.CreateBusView.as_view(),
        name='crear'
    ),
    path(
        route='editar/<int:pk>',
        view=views.UpdateBusView.as_view(),
        name='editar'
    ),
    path(
        route='eliminar/<int:pk>',
        view=views.DeleteBusView.as_view(),
        name='eliminar'
    ),
    path(
        route='filtro/',
        view=views.busesFiltro,
        name='filtro'
    ),

]
