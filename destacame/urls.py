"""destacame URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include

""" Rutas Apps Views """
from destacame import views as local_views
from choferes import views as choferes_views
from buses import views as buses_views
from trayectos import views as trayectos_views
from pasajeros import views as pasajeros_views

urlpatterns = [
    
    path('choferes/', include(('choferes.urls', 'choferes'), namespace='choferes')),
    path('buses/', include(('buses.urls', 'buses'), namespace='buses')),
    path('trayectos/', include(('trayectos.urls', 'trayectos'), namespace='trayectos')),
    path('pasajeros/', include(('pasajeros.urls', 'pasajeros'), namespace='pasajeros')),
    path('admin/', admin.site.urls),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
