
from django.http import HttpResponse
from datetime import datetime
import json

def hello_world(request):
    now = datetime.now().strftime('%b %dth, %Y - %H:%M hrs')
    return HttpResponse('Hello,world. La fecha es {now}'.format(now=now))

def sortedTest(request):
    print(request)
    #import pdb; pdb.set_trace()
    numbers = [int(i) for i in request.GET['numbers'].split(',')]
    sorted_ints = sorted(numbers)
    data = {
        'status':'ok',
        'numbers': sorted_ints,
        'message': 'Pruebas'
    }
#    return HttpResponse(str(numbers), content_type='application/json')
    return HttpResponse(
        json.dumps(data,indent=4),
        content_type='application/json')

def paramsTest(request,name,age):
    if age < 12:
        message = 'No estas permidito {}'.format(name)
    else:
        message = 'Hola, {} bienvenido'.format(name)
    return HttpResponse(message)

