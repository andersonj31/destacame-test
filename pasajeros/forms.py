"""Post forms."""

# Django
from django import forms

# Models
from pasajeros.models import Pasajero


class PasajeroForm(forms.ModelForm):

    class Meta:
        model = Pasajero
        fields = ('rut','nombre','apellido','telefono','num_asiento','trayecto_bus')
