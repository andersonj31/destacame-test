"""Posts URLs."""

# Django
from django.urls import path

# Views
from pasajeros import views

urlpatterns = [
    path(
        route='',
        view=views.PasajeroView.as_view(),
        name='list'
    ),
    path(
        route='<int:pk>',
        view=views.DetailPasajeroView.as_view(),
        name='detalle'
    ),
    path(
        route='crear',
        view=views.CreatePasajeroView.as_view(),
        name='crear'
    ),
    path(
        route='editar/<int:pk>',
        view=views.UpdatePasajeroView.as_view(),
        name='editar'
    ),
    path(
        route='eliminar/<int:pk>',
        view=views.DeletePasajeroView.as_view(),
        name='eliminar'
    ),

    #AJAX METHODS
    path(
        route='getTrayectosBuses',
        view=views.getTrayectosBuses,
        name='get_trayectos_buses'
    ),
    path(
        route='getAsientos',
        view=views.getAsientos,
        name='get_asientos'
    ),

]
