# Django
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import CreateView, DetailView, ListView, UpdateView, DeleteView
from django.template import loader, Context
from django.core import serializers

# Forms
from pasajeros.forms import PasajeroForm
from trayectos.forms import TrayectoForm

# Models
from trayectos.models import Trayecto, TrayectoBuses
from buses.models import Bus
from pasajeros.models import Pasajero

class PasajeroView(ListView):

    template_name = 'pasajeros/index.html'
    model = Pasajero
    ordering = ('-created',)
    paginate_by = 30
    context_object_name = 'pasajeros'


class DetailPasajeroView(DetailView):

    model = Pasajero
    template_name = 'pasajeros/view.html'

class CreatePasajeroView(CreateView):

    model = Pasajero
    form_class = PasajeroForm
    template_name = 'pasajeros/create.html'
    success_url = reverse_lazy('pasajeros:list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['trayectos'] = Trayecto.objects.all()
        return context


class UpdatePasajeroView(UpdateView):

    model = Pasajero
    template_name = 'pasajeros/update.html'
    form_class = PasajeroForm
    success_url = reverse_lazy('pasajeros:list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['trayectos'] = Trayecto.objects.all()
        context['horarios'] = TrayectoBuses.objects.filter(trayecto_id = self.object.trayecto_bus.trayecto)
        context['asientos'] = Pasajero.objects.filter(trayecto_bus = self.object.trayecto_bus).values('num_asiento')
        context['listAsientos'] = [1,2,3,4,5,6,7,8,9,10]

        return context

class DeletePasajeroView(DeleteView):

    model = Pasajero
    form_class = PasajeroForm
    success_url = reverse_lazy('pasajeros:list')

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

def getTrayectosBuses(request):
	lista = serializers.serialize('json', TrayectoBuses.objects.filter(trayecto_id = request.POST['trayecto_id']))
	return HttpResponse(lista, content_type='application/json')

def getAsientos(request):
	lista = serializers.serialize('json', Pasajero.objects.filter(trayecto_bus = request.POST['trayecto_bus']))
	return HttpResponse(lista, content_type='application/json')

"""def asignarHorario(request,pk):
    trayecto = Trayecto.objects.get(id=pk)
    buses = Bus.objects.all()
    horariosAsigandos = TrayectoBuses.objects.filter(trayecto_id = pk)
    error_msg = False
    form = HorariosForm        
    if request.method == 'POST':
        cargado = TrayectoBuses.objects.filter(trayecto_id = pk,bus_id = request.POST['bus'] ).count()
        if cargado == 0:
            form = HorariosForm(request.POST)
            if form.is_valid():
                form.save()
        else:
            error_msg = "El Bus ya se encuentra asignado a un horario"

    contexto = {'form':form,
        'trayecto':trayecto,
        'buses':buses,
        'horariosAsigandos':horariosAsigandos,
        'error_msg': error_msg 
    }
    return render(request, 'trayectos/horarios.html', contexto)

def borrarHorario(request,trayecto,horario):
    TrayectoBuses.objects.get(id = horario).delete()
    return redirect('trayectos:asignar',trayecto)"""


