from django.db import models

# Create your models here.

class Pasajero(models.Model):

    rut = models.IntegerField(max_length=15)
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    telefono = models.IntegerField()
    num_asiento = models.IntegerField()
    trayecto_bus = models.ForeignKey(
        'trayectos.TrayectoBuses',
        on_delete=models.CASCADE,
    )
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)